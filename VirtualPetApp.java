import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){

		Scanner reader = new Scanner(System.in);

		// making an arry of 4 birds

	Parrots [] flock = new Parrots [4];

	// adding 4 types of birds in this arry

	int number=0;

	for (int i =0; i <flock.length; i++){

		// this sets the null into each bird's object

		flock [i] = new Parrots();

		// ask the user for his bird's name

		System.out.println("Enter the name of your bird");

		// asking user for his bird's name

		flock [i].name= reader.nextLine();

		System.out.println("Enter your bird's color");

		// asking user for his birds color

		flock [i].colour= reader.nextLine();

		System.out.println("Enter if bird can talk. Enter 1 for yes, 2 for No");

		// asking user to enter 1 to say if his/her bird talks or not 

		number= flock[i].talksOrNot= Integer.parseInt(reader.nextLine());



	}

	// calling the field of the Parrots class

	System.out.println("Your bird's name is: "+flock[3].name);
	System.out.println("Your bird's color is: "+flock[3].colour);
	System.out.println(flock[3].canTalk);

	// calling the instance methods from Parrots class to the user 2
	
	flock[1].fly();
	flock[1].talks(number);
	flock[1].color();


	}
	
}